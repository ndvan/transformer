from datasets import load_dataset
from transformers import Trainer, TrainingArguments
from transformers import DataCollatorForLanguageModeling

from transformers import GPT2Tokenizer, GPT2LMHeadModel

import transformers
transformers.logging.set_verbosity_info()

tokenizer = GPT2Tokenizer.from_pretrained('gpt2')
tokenizer.pad_token = tokenizer.eos_token

datasets = load_dataset('text', data_files={'train': ['data.txt'], 'eval': ['eval.txt']})

def tokenize_function(examples):
    return tokenizer(examples['text'])
        # , add_special_tokens=True, padding=True)
        # padding='max_length')

tokenized_datasets = datasets.map(
    tokenize_function,
    batched=True,
    batch_size=5
)

# print("=============")
# print(tokenized_datasets['train'])
# print(tokenized_datasets['train'][0])
# print(tokenizer.decode(tokenized_datasets['train'][0]['input_ids']))
# for i in tokenized_datasets['train'][0]['input_ids']:
#     print(i)
#     print(tokenizer.decode(i))
# print("=============")
# breakpoint()

data_collator = DataCollatorForLanguageModeling(
    tokenizer=tokenizer, mlm=False, 
    # mlm_probability=0.15
)

training_args = TrainingArguments(
    output_dir="./VanGPT",
    overwrite_output_dir=True,
    num_train_epochs=2,
    logging_steps=10
)

model = GPT2LMHeadModel.from_pretrained('gpt2')

# inputs = tokenizer("Hello, my dog is cute", return_tensors="pt")
# outputs = model(**inputs, labels=inputs["input_ids"])
# loss = outputs.loss
# logits = outputs.logits
# print(loss, logits)

trainer = Trainer(
    model=model,
    args=training_args,
    data_collator=data_collator,
    train_dataset=tokenized_datasets['train'],
    eval_dataset=tokenized_datasets['eval'],
)

trainer.train()

trainer.save_model("./VanGPT")

for i in range(3):
    print("=== Running #" + str(i))
    model = GPT2LMHeadModel.from_pretrained("VanGPT")

    trainer = Trainer(
        model=model,
        args=training_args,
        data_collator=data_collator,
        train_dataset=tokenized_datasets['train'],
        eval_dataset=tokenized_datasets['eval'],
    )

    trainer.train()

    trainer.save_model("./VanGPT")
