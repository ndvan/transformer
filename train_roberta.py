from datasets import load_dataset
from transformers import Trainer, TrainingArguments
from transformers import RobertaConfig
from transformers import RobertaTokenizerFast
from transformers import RobertaForCausalLM
from transformers import RobertaForMaskedLM
from transformers import DataCollatorForLanguageModeling
from transformers import LineByLineTextDataset
from transformers import BertTokenizer
from transformers import pipeline

import transformers
transformers.logging.set_verbosity_info()

# config = RobertaConfig(
    # vocab_size=5000,
    # max_position_embeddings=514,
    # num_attention_heads=12,
    # num_hidden_layers=6,
    # type_vocab_size=1,
# )

# tokenizer = RobertaTokenizerFast.from_pretrained("./VanBERT", max_len=512)
tokenizer = RobertaTokenizerFast.from_pretrained("roberta-base")

# model = RobertaForMaskedLM(
  # config=config
  # )
# model = RobertaForMaskedLM.from_pretrained("VanBERT")

datasets = load_dataset('text', data_files={'train': ['data.txt'], 'eval': ['eval.txt']})
# dataset = LineByLineTextDataset(
#     tokenizer=tokenizer,
#     file_path="./text.txt",
#     block_size=128,
# )

def tokenize_function(examples):
    return tokenizer(examples['text'], add_special_tokens=True, padding=True)
      # padding='max_length')

tokenized_datasets = datasets.map(
    tokenize_function,
    batched=True,
    batch_size=5
)

# print("=============")
# print(tokenized_datasets['train'])
# print(tokenized_datasets['train'][0])
# print(tokenizer.decode(tokenized_datasets['train'][0]['input_ids']))
# for i in tokenized_datasets['train'][0]['input_ids']:
#   print(i)
#   print(tokenizer.decode(i))
# print("=============")
# breakpoint()

data_collator = DataCollatorForLanguageModeling(
    tokenizer=tokenizer, mlm=False, 
    # mlm_probability=0.15
)

training_args = TrainingArguments(
    output_dir="./VanBERT",
    logging_dir="./logs",
    overwrite_output_dir=True,
    num_train_epochs=2,
    logging_steps=10
)

#model = RobertaForCausalLM.from_pretrained("roberta-base")

#trainer = Trainer(
#  model=model,
#  args=training_args,
#  data_collator=data_collator,
#  train_dataset=tokenized_datasets['train'],
#  eval_dataset=tokenized_datasets['eval'],
#)

#trainer.train()

#trainer.save_model("./VanBERT")

for i in range(1):
    print("=== Running #" + str(i))
    model = RobertaForCausalLM.from_pretrained("VanBERT")

    trainer = Trainer(
        model=model,
        args=training_args,
        data_collator=data_collator,
        train_dataset=tokenized_datasets['train'],
        eval_dataset=tokenized_datasets['eval'],
    )

    trainer.train()

    trainer.save_model("./VanBERT")

# x = tokenizer.encode("Hello general Van", add_special_tokens = True)
# print(x)
# print(tokenizer.decode(x))
