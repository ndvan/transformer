clingo scheduler/all_possible.lp --outf=0 -V0 --out-atomf=%s --quiet=1,2,2 | head -n1 > atoms.txt

sed -E -e 's/[[:blank:]]+/\n/g' atoms.txt > lineatoms.txt

awk '{
  # if (match($0,/device\(/)) {
  #   if (match($0, /\d+/)) {
  #     device = substr($0,RSTART,RLENGTH)
  #     str = $0 " has parameter " device
  #   }
  #   print str "<s>There is device " device
  # } else if (match($0,/timeslot\(/)) {
  #   if (match($0, /\d+/)) {
  #     timeslot = substr($0,RSTART,RLENGTH)
  #     str = $0 " has parameter " timeslot
  #   }
  #   print str "<s>There is timeslot " timeslot
  # } else 
  if (match($0,/on\(/)) {
    str = $0 " has parameters "
    match($0, /[0-9]+,[0-9]+/)
    paramstr = substr($0,RSTART,RLENGTH)
    split(paramstr, params, ",")
    str = str params[1] " " params[2]
    print str " turn on<s>Device " params[1] " is on at timeslot " params[2]
  } else if (match($0,/weight\(/)) {
    str = $0 " has parameters "
    match($0, /[0-9]+,[0-9]+,[0-9]+,[0-9]+/)
    paramstr = substr($0,RSTART,RLENGTH)
    split(paramstr, params, ",")
    str = str params[1] " " params[2] " " params[3] " " params[4]
    print str " weight<s>Device " params[1] " consumes " params[3] " kWh and the cost at timeslot " params[2] " is " params[4] " cents per hour"
  } else if (match($0,/costthr\(/)) {
    str = $0 " has parameters "
    match($0, /[0-9]+,[0-9]+/)
    paramstr = substr($0,RSTART,RLENGTH)
    split(paramstr, params, ",")
    str = str params[1] " " params[2]
    print str " cost<s>The total cost of the schedule is " params[1]
  } else if (match($0,/energythr\(/)) {
    str = $0 " has parameters "
    match($0, /[0-9]+,[0-9]+,[0-9]+/)
    paramstr = substr($0,RSTART,RLENGTH)
    split(paramstr, params, ",")
    str = str params[1] " " params[2] " " params[3]
    print str " energy threshold<s>Energy consumption at timeslot " params[2] " is " params[1]
  } else if (match($0,/condibefore\(/)) {
    str = $0 " has parameters "
    match($0, /[0-9]+,[0-9]+,[0-9]+,[0-9]+/)
    paramstr = substr($0,RSTART,RLENGTH)
    split(paramstr, params, ",")
    str = str params[1] " " params[2] " " params[3] " " params[4]
    print str " condition before<s>Device " params[1] " turns on at " params[3] " before device " params[2] " at " params[4]
  } else if (match($0,/condiafter\(/)) {
    str = $0 " has parameters "
    match($0, /[0-9]+,[0-9]+,[0-9]+,[0-9]+/)
    paramstr = substr($0,RSTART,RLENGTH)
    split(paramstr, params, ",")
    str = str params[1] " " params[2] " " params[3] " " params[4]
    print str " condition after<s>Device " params[1] " turns on at " params[3] " after device " params[2] " at " params[4]
  } else if (match($0,/condiparallel\(/)) {
    str = $0 " has parameters "
    match($0, /[0-9]+,[0-9]+,[0-9]+/)
    paramstr = substr($0,RSTART,RLENGTH)
    split(paramstr, params, ",")
    str = str params[1] " " params[2] " " params[3]
    print str " same<s>Device " params[1] " and " params[2] " turn on at " params[3] 
  } else if (match($0,/condinparallel\(/)) {
    str = $0 " has parameters "
    match($0, /[0-9]+,[0-9]+,[0-9]+,[0-9]+/)
    paramstr = substr($0,RSTART,RLENGTH)
    split(paramstr, params, ",")
    str = str params[1] " " params[2] " " params[3] " " params[4]
    print str " different<s>Device " params[1] " turns on at " params[3] ", and device " params[2] " at " params[4]
  } else if (match($0,/condipref\(/)) {
    str = $0 " has parameters "
    match($0, /[0-9]+,[0-9]+/)
    paramstr = substr($0,RSTART,RLENGTH)
    split(paramstr, params, ",")
    str = str params[1] " " params[2]
    print str " preference<s>The estimated preference of the schedule is " params[1] " and " params[2]
  }
}' lineatoms.txt > data.txt
