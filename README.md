## Train the language model:

```
python train.py
```

## Generation test:

```
python gen.py [gpt or bert]
```

## Generate data for training from ASP:

```
./gen_data.sh
```

data is generated in _data.txt_

## Given a schedule, generate explanations by

```
clingo explain.lp
```

```
clingo q1.lp
```

```
./q2.sh
```

There are 2 parts in answering q2. Check the script for more detail.

The default LM is GPT-2, which is in folder _VGPT_. Change _VGPT_ to _VanGPT_ to let the script access the model if you first clone this repo.

## Requirements

- conda
- transformer (hugging face)
- clingo
- nltk (for BLUE evaluation)

```
$ conda list
# packages in environment at p/a/t/h:
#
# Name                    Version                   Build  Channel
_pytorch_select           0.1                       cpu_0  
arrow-cpp                 3.0.0            py37hf7c73f6_0  
blas                      1.0                         mkl  
boost-cpp                 1.73.0              h9ed2024_11  
brotli                    1.0.9                hb1e8313_2  
brotlipy                  0.7.0           py37h9ed2024_1003  
bzip2                     1.0.8                h1de35cc_0  
c-ares                    1.17.1               h9ed2024_0  
ca-certificates           2021.1.19            hecd8cb5_1  
certifi                   2020.12.5        py37hecd8cb5_0  
cffi                      1.14.0           py37hb5b8e2f_0  
chardet                   4.0.0           py37hecd8cb5_1003  
click                     7.1.2              pyhd3eb1b0_0  
clingo                    5.4.0           py37lua53h0a44026_0    potassco
cryptography              3.3.1            py37hbcfaee0_0  
dataclasses               0.7                      py37_0  
datasets                  1.2.1                      py_0    huggingface
dill                      0.3.3              pyhd3eb1b0_0  
double-conversion         3.1.5                haf313ee_1  
filelock                  3.0.12             pyhd3eb1b0_1  
gflags                    2.2.2                h0a44026_0  
glog                      0.4.0                h0a44026_0  
grpc-cpp                  1.26.0               h044775b_0  
icu                       58.2                 h0a44026_3  
idna                      2.10               pyhd3eb1b0_0  
importlib-metadata        2.0.0                      py_1  
importlib_metadata        2.0.0                         1  
intel-openmp              2019.4                      233  
joblib                    1.0.0              pyhd3eb1b0_0  
libboost                  1.73.0              hd4c2dcd_11  
libcxx                    10.0.0                        1  
libedit                   3.1.20191231         h1de35cc_1  
libevent                  2.1.8                hddc9c9b_1  
libffi                    3.2.1             h0a44026_1007  
libiconv                  1.16                 h1de35cc_0  
libmklml                  2019.0.5                      0  
libprotobuf               3.11.2               hd9629dc_0  
libthrift                 0.13.0               h054ceb0_6  
llvm-openmp               10.0.0               h28b9765_0  
lua                       5.3.4                h1de35cc_0  
lz4-c                     1.9.3                h23ab428_0  
mkl                       2019.4                      233  
mkl-service               2.3.0            py37h9ed2024_0  
mkl_fft                   1.2.0            py37hc64f4ea_0  
mkl_random                1.1.1            py37h959d312_0  
multiprocess              0.70.11.1        py37hf967b71_1    conda-forge
ncurses                   6.2                  h0a44026_1  
ninja                     1.10.2           py37hf7b0b51_0  
numpy                     1.19.2           py37h456fd55_0  
numpy-base                1.19.2           py37hcfb5961_0  
openssl                   1.1.1k               h9ed2024_0  
orc                       1.6.5                h001ef8f_1  
packaging                 20.9               pyhd3eb1b0_0  
pandas                    1.2.1            py37hb2f4e1b_0  
pip                       20.3.3           py37hecd8cb5_0  
protobuf                  3.11.2           py37h0a44026_0  
pyarrow                   3.0.0            py37hdf3e9eb_0  
pycparser                 2.20                       py_2  
pyopenssl                 20.0.1             pyhd3eb1b0_1  
pyparsing                 2.4.7              pyhd3eb1b0_0  
pysocks                   1.7.1            py37hecd8cb5_0  
python                    3.7.7           hfe9666f_0_cpython  
python-dateutil           2.8.1              pyhd3eb1b0_0  
python-xxhash             2.0.0            py37hf967b71_1    conda-forge
python_abi                3.7                     1_cp37m    huggingface
pytorch                   1.6.0           cpu_py37hd70000b_0  
pytz                      2020.5             pyhd3eb1b0_0  
re2                       2020.11.01           h23ab428_1  
readline                  7.0                  h1de35cc_5  
regex                     2020.11.13       py37h9ed2024_0  
requests                  2.25.1             pyhd3eb1b0_0  
sacremoses                master                     py_0    huggingface
setuptools                52.0.0           py37hecd8cb5_0  
six                       1.15.0           py37hecd8cb5_0  
snappy                    1.1.8                hb1e8313_0  
sqlite                    3.33.0               hffcf06c_0  
tk                        8.6.10               hb0a8c7a_0  
tokenizers                0.9.4                    py37_0    huggingface
tqdm                      4.49.0                     py_0  
transformers              4.2.2              pyh7b7c402_0    huggingface
uriparser                 0.9.3                h0a44026_1  
urllib3                   1.26.3             pyhd3eb1b0_0  
utf8proc                  2.6.1                h9ed2024_0  
wheel                     0.36.2             pyhd3eb1b0_0  
xxhash                    0.8.0                h35c211d_3    conda-forge
xz                        5.2.5                h1de35cc_0  
zipp                      3.4.0              pyhd3eb1b0_0  
zlib                      1.2.11               h1de35cc_3  
zstd                      1.4.5                h41d2c2f_0 
```

#### Just some tips for working in nmsu server

- Generate all possible atoms (given a problem instance in the _example_ folder):

```
clingo scheduler/all_possible.lp --outf=0 -V0 --out-atomf=%s --quiet=1,2,2 | head -n1
```

to check disk space

du -lsh *

===============byobu=============== 
scroll up and down

Ctrl-A [

This will activate copy mode in GNU/screen. Now, you can scroll up/down and look at your data. Use the following keys:

Ctrl-u and Ctrl-d scroll the display up/down by the specified amount of lines while preserving the cursor position. (Default: half screen-full).
Ctrl-b and Ctrl-f scroll the display up/down a full screen.
From the man page - following movement keys can be used in copy mode:

h, j, k, l move the cursor line by line or column by column.
0, ^ and $ move to the leftmost column, to the first or last non-whitespace character on the line.
H, M and L move the cursor to the leftmost column of the top, center or bottom line of the window.
+ and - positions one line up and down.
G moves to the specified absolute line (default: end of buffer).
| moves to the specified absolute column.
w, b, e move the cursor word by word.
B, E move the cursor WORD by WORD (as in vi).
C-u and C-d scroll the display up/down by the specified amount of lines while preserving the cursor position. (Default: half screen-full).
C-b and C-f scroll the display up/down a full screen.
g moves to the beginning of the buffer.
% jumps to the specified percentage of the buffer.
For searching use the following vi like syntax:

/SearchWord - Vi-like search forward.
?SearchWord - Vi-like search backward.

===================================