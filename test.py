from datasets import load_dataset
from transformers import AutoTokenizer, AutoModelForCausalLM, Trainer, TrainingArguments, pipeline, set_seed

datasets = load_dataset('wikitext', 'wikitext-2-raw-v1')

model_checkpoint = "distilgpt2"
tokenizer = AutoTokenizer.from_pretrained(model_checkpoint, use_fast=True)
model = AutoModelForCausalLM.from_pretrained(model_checkpoint)

def tokenize_function(examples):
    return tokenizer(examples["text"])

tokenized_datasets = datasets.map(tokenize_function, batched=True, num_proc=4, remove_columns=["text"])

# print("tokenizer_datasets\n-----")
# print(tokenized_datasets["train"][0])
# print("----")

block_size = 16

def group_texts(examples):
    # Concatenate all texts.
    concatenated_examples = {k: sum(examples[k], []) for k in examples.keys()}
    # print(examples)
    # print(tokenizer.decode(examples["input_ids"][0]))
    total_length = len(concatenated_examples[list(examples.keys())[0]])
    # We drop the small remainder, we could add padding if the model supported it instead of this drop, you can
        # customize this part to your needs.
    total_length = (total_length // block_size) * block_size
    # Split by chunks of max_len.
    result = {
        k: [t[i : i + block_size] for i in range(0, total_length, block_size)]
        for k, t in concatenated_examples.items()
    }
    # print(result)
    result["labels"] = result["input_ids"].copy()
    return result

lm_datasets = tokenized_datasets.map(
    group_texts,
    batched=True,
    batch_size=1000,
    num_proc=4,
)

# print(tokenizer.decode(lm_datasets["train"][1]["input_ids"]))

training_args = TrainingArguments(
    "test-clm",
    evaluation_strategy = "epoch",
    learning_rate=2e-5,
    weight_decay=0.01,
)

trainer = Trainer(
    model=model,
    args=training_args,
    train_dataset=lm_datasets["train"],
    eval_dataset=lm_datasets["validation"],
)

breakpoint()
quit()

trainer.train()

# prompt = "atom_1 has"

prompt = "atom_5 zzz robot"
inputs = tokenizer.encode(prompt, add_special_tokens=True, return_tensors="pt")
prompt_length = len(tokenizer.decode(inputs[0], skip_special_tokens=True, clean_up_tokenization_spaces=True))

# beam_outputs = model.generate(
#                     inputs, 
#                     max_length=50, 
#                     num_beams=5, 
#                     num_return_sequences=5, 
#                     early_stopping=True
# )

sample_output = model.generate(
    inputs, 
    do_sample=True, 
    max_length=50, 
    top_p=0.92, 
    top_k=0,
    num_return_sequences=5, 
    early_stopping=True
)

print("Output:\n" + 100 * '-')

for i, output in enumerate(sample_output):
    print("{}: {}".format(i, tokenizer.decode(output, skip_special_tokens=True)))
