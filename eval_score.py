import nltk
from transformers import GPT2Tokenizer, GPT2LMHeadModel
import sys
import re

filename = "data.txt"
tokenizer = GPT2Tokenizer.from_pretrained('gpt2')
model = GPT2LMHeadModel.from_pretrained("./VanGPT", pad_token_id=tokenizer.eos_token_id)

file = open(filename, "r")
line = file.readline()

atom_count = 0
total_BLUE_score = 0

while line:
    a = line.split()[0]
    print(a)
    extra = ""
    x = re.findall("\d+", a)
    # print(x)
    prompt = "<s>" + a + " has parameters " + " ".join(x)
    if "on(" in a:
        extra = " turn on"
    elif "weight(" in a:
        extra = " weight"
    elif "costthr" in a:
        extra = " cost"
    elif "energythr" in a:
        extra = " energy threshold"
    elif "condibefore" in a:
        extra = " condition before"
    elif "condiafter" in a:
        extra = " condition after"
    elif "condiparallel" in a:
        extra = " same"
    elif "condinparallel" in a:
        extra = " different"
    elif "condipref" in a:
        extra = " preference"
    elif "device(" in a or "timeslot(" in a:
        continue
    prompt = prompt + extra + "<s>"
    cut_point = len(prompt)

    inputs = tokenizer.encode(prompt, add_special_tokens=False, return_tensors="pt")

    greedy_output = model.generate(inputs, max_length=50)
    generated = tokenizer.decode(greedy_output[0], skip_special_tokens=True)
    output = generated[cut_point:]
    # print(output)

    hypothesis = line.split()
    reference = output.split()
    BLEUscore = nltk.translate.bleu_score.sentence_bleu([reference], hypothesis)
    total_BLUE_score += BLEUscore
    atom_count += 1

    line = file.readline()
file.close()

print("BLEUscore: {} / {} = {}".format(total_BLUE_score, atom_count, total_BLUE_score / atom_count))

