from transformers import RobertaTokenizerFast
from transformers import RobertaForCausalLM
from transformers import RobertaForMaskedLM
from transformers import pipeline
from transformers import BertTokenizer
from transformers import GPT2Tokenizer, GPT2LMHeadModel
import sys

if sys.argv[1] == "bert":
    # model = RobertaForMaskedLM.from_pretrained("./VanBERT")
    model = RobertaForCausalLM.from_pretrained("./VanBERT")

    # tokenizer = RobertaTokenizerFast.from_pretrained("./VanBERT", max_len=512)
    tokenizer = RobertaTokenizerFast.from_pretrained("roberta-base")
elif sys.argv[1] == "gpt":
    tokenizer = GPT2Tokenizer.from_pretrained('gpt2')
    model = GPT2LMHeadModel.from_pretrained("./VanGPT", pad_token_id=tokenizer.eos_token_id)


prompt = ["<s>weight(1,1,30,202) has parameters 1 1 30 202<s>Device", "<s>condipref(7,36) has parameters 7 36<s>The", "<s>energythr(3,22,32) has parameters 3 22 32<s>Energy","<s>condibefore(2,1,2,6) has parameters 2 1 2 6<s>"]

for i in range(len(prompt)):
    input_ids = tokenizer.encode(prompt[i], return_tensors="pt")
    # prompt_length = len(tokenizer.decode(inputs[0], skip_special_tokens=True, clean_up_tokenization_spaces=True))

    # logits_processor = LogitsProcessorList([
    #     MinLengthLogitsProcessor(30),
    # ])
    # print(inputs)

    # print("len inputs: " + str(len(inputs[0])))
    greedy_output = model.generate(input_ids, max_length=50)
    print("Output:\n" + 100 * '-')
    print(tokenizer.decode(greedy_output[0], skip_special_tokens=True))

    # greedy_output = model.greedy_search(
    #     inputs, 
    #     # max_length=50,
    #     # early_stopping=True,
    # )
    # print(greedy_output)
    # print("len greedy_output: " + str(len(greedy_output[0])))
    # print("Output greedy:\n" + 100 * '-')
    # print(prompt[i])
    # print(tokenizer.decode(greedy_output[0]))
    # prompt_out = tokenizer.batch_decode(greedy_output)[0]
    # print("Generated:", prompt_out)
    # , skip_special_tokens=True



    # beam_outputs = model.generate(
    #     inputs, 
    #     max_length=50, 
    #     num_beams=5, 
    #     num_return_sequences=5, 
    #     early_stopping=True
    # )

    # print("Output beam:\n" + 100 * '-')

    # for i, output in enumerate(beam_outputs):
    #     print("{}: {}".format(i, tokenizer.decode(output, skip_special_tokens=True)))

    # fill_mask = pipeline(
    #     "fill-mask",
    #     model="./VanBERT",
    #     tokenizer=tokenizer
    # )

    # print(fill_mask("the arts as <mask> activities"))
    # print(fill_mask("<s>condipref(7,36) has parameters 7 36<s>The estimated preference of the schedule is 7 <mask> 36</s>"))

